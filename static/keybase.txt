==================================================================
https://keybase.io/mochimoon
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://mochimoon.gitlab.io
  * I am mochimoon (https://keybase.io/mochimoon) on keybase.
  * I have a public key ASCmBC9Bi2ZjbO6eiMdxUp0nN9WoC2a-34CTVA1ZBI6GJwo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0120a6042f418b66636cee9e88c771529d2737d5a80b66bedf8093540d59048e86270a",
      "host": "keybase.io",
      "kid": "0120a6042f418b66636cee9e88c771529d2737d5a80b66bedf8093540d59048e86270a",
      "uid": "11f68cf3a21965dade2de714b745b419",
      "username": "mochimoon"
    },
    "merkle_root": {
      "ctime": 1577584363,
      "hash": "e9a96b3e4555ba980f9b8a0b860ae2e72192fa4a78f81e50730f19ae234983516f7a3767426c84f537834c6cf370981d52e03fc0345ada48809451b24c658448",
      "hash_meta": "39633471c914f0533a2239ed383ee6cd53adf90cd95bf4d8442861279cf6b6c4",
      "seqno": 13997492
    },
    "service": {
      "entropy": "2QQkW6pG6V5mX2PzxNhAQaTa",
      "hostname": "mochimoon.gitlab.io",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "5.1.1"
  },
  "ctime": 1577584391,
  "expire_in": 504576000,
  "prev": "ad8d9c482811a78b2ccfd2f385fc955a175c9d015a366c471f52c4b52f150e50",
  "seqno": 7,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgpgQvQYtmY2zunojHcVKdJzfVqAtmvt+Ak1QNWQSOhicKp3BheWxvYWTESpcCB8QgrY2cSCgRp4ssz9LzhfyVWhdcnQFaNmxHH1LEtS8VDlDEIHNZhUf8AUvYtPv3ByrR64SyGYTn+LiSpKmeRdiNDsnrAgHCo3NpZ8RAOcTZmP0+U06KNll7zmb/UpN8PANKOTRPfNG3UyUaMz5yHh4w4DJauXzpzlKvkV5ltG5u6G/mn14I5d+mjfgiDahzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIFBHR8WdZGYHtcuXjHpEq1KgVaZItcI09KoVsG496u/To3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/mochimoon

==================================================================
